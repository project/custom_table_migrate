
INTRODUCTION
------------

The Custom Table Migrate module provides a feature to  Migrate
 Custom table from MYSQL to Mongodb. 

REQUIREMENTS
------------

This module requires the following module:

 * MongoDB (https://www.drupal.org/project/mongodb)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.
Extract the 'custom_table_migrate' folder and put in to modules directory

CONFIGURATION
-------------

  * Configure settings at Admin » Structure » Migrate Custom Table

  - OR After installation, click on structure » Migrate Custom Table » 
    Enter Custom Table Name which you want to Migrate Mysql to Mongodb.
