<?php

/**
 * @file
 * Provides functions for Administration settings.
 */

/**
 * Migrate Custom table form.
 *
 * @ingroup forms
 */
function custom_table_migrate_data_port_to_mongodb_form($form, &$form_state) {
  $form['custom_table_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter Table Name without Prefix'),
    '#description' => t('Please Enter Table Name to Port Mongodb'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Custom_table_migrate_data_port_to_mongodb_form validate.
 *
 * @ingroup forms
 */
function custom_table_migrate_data_port_to_mongodb_form_validate($form, &$form_state) {
  $tble = $form_state['values']['custom_table_name'];
  if (isset($tble)) {
    // Checking table exist or not.
    $result = db_table_exists($tble);
    if ($result != 1) {
      form_set_error('custom_table_name', t('Table Does Not Exist!'));
    }
  }
  else {
    form_set_error('custom_table_name', t('Enter Custom Table Name!'));
  }
}

/**
 * Custom_table_migrate_data_port_to_mongodb_form submit.
 *
 * @ingroup forms
 */
function custom_table_migrate_data_port_to_mongodb_form_submit($form, &$form_state) {
  $tablename = $form_state['values']['custom_table_name'];
  $mysql_int_types = array(
    'TINYINT',
    'SMALLINT',
    'MEDIUMINT',
    'INT',
    'BIGINT',
    'BIT',
  );
  $mysql_float_types = array(
    'DECIMAL',
    'FLOAT',
    'DOUBLE',
  );
  // Fetch Table's Data form $tablename.
  $result = db_query('SELECT * FROM {'. $tablename . '}');

  foreach ($result as $value) {
    $operations[] = array(
      "custom_table_migrate_batch_porting_mysql_table_to_mongodb",
       array($value, $tablename, $mysql_int_types, $mysql_float_types),
    );
  }
  $batch = array(
    'operations' => $operations,
    'finished' => 'custom_table_migrate_batch_porting_mysql_table_to_mongodb_finished',
    'title' => t('Port mysql Tabel to Mongodb'),
    'init_message' => t('port mysql table to mongodb.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('port mysql table to mongodb Batch has encountered an error.'),
  );
  batch_set($batch);
  batch_process(drupal_get_destination());
}

/**
 * Batch finished function.
 *
 * @param bool $success
 *   Success is return true or false.
 * @param int $results
 *   Result is show total item process.
 * @param mixed $operations
 *   Operations contains the operations that remained unprocessed.
 */
function custom_table_migrate_batch_porting_mysql_table_to_mongodb_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = t('@count items successfully processed', array('@count' => count($results)));
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // Operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments',
        array(
          '%error_operation' => $error_operation[0],
          '@arguments' => print_r($error_operation[1], TRUE),
        )
        );
    drupal_set_message($message, 'error');
  }
}
